import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneResultUnitComponent } from './phone-result-unit.component';

describe('PhoneResultUnitComponent', () => {
  let component: PhoneResultUnitComponent;
  let fixture: ComponentFixture<PhoneResultUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneResultUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneResultUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
