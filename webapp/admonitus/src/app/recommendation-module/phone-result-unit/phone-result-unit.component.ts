import { Component, Input, OnInit } from '@angular/core';
import { Phone } from '../Phone';

@Component({
  selector: 'app-phone-result-unit',
  templateUrl: './phone-result-unit.component.html',
  styleUrls: ['./phone-result-unit.component.css']
})
export class PhoneResultUnitComponent implements OnInit {

  constructor() { }

  @Input() phone:Phone;
  ngOnInit() {
  }

}
