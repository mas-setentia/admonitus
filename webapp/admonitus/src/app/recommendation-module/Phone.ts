import { Manufacturer } from '../Models/Manufacturer';
import { Specification } from '../Models/Specification';

export class Phone {
    private modelName:string;

    private variantName:string;

    private manufacturer:Manufacturer;

    private phoneSpecification:Specification;

} 