import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatChip, MatChipInputEvent, MatChipList } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Options, LabelType} from 'ng5-slider';
import { RecommendationServiceService } from '../recommendation-service.service';
import { ThrowStmt } from '@angular/compiler';
import { Router } from '@angular/router';
import { User } from '../User';

@Component({
  selector:"app-do-not-use-spinner",
  template: `<mat-spinner></mat-spinner>`,
  styles: []
})
export class DialogSpinnerComponent{}


@Component({
  selector: 'app-recommendation-form',
  templateUrl: './recommendation-form.component.html',
  styleUrls: ['./recommendation-form.component.css']
})
export class RecommendationFormComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  totalSelected: number = 0;
  Priority: Array<number> = [];
  options:Options= {
    floor :500,
    ceil:200000,
    step: 500,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '<b>Min price:</b> $' + value;
        case LabelType.High:
          return '<b>Max price:</b> $' + value;
        default:
          return '$' + value;
      }
    }
  };
  value:number = 10000;
  maxValue: number = 20000;
  private name: String = "";
  private dialog:MatDialogRef<DialogSpinnerComponent>;
  user:User = new User();
  constructor(private _formBuilder: FormBuilder,private matDialog: MatDialog,private recommendationService:RecommendationServiceService
    ,private route: Router) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      name: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      EmailId: ['', [Validators.required,Validators.email]]
    });
    this.thirdFormGroup = this._formBuilder.group({
      lowRange: [""],
      highRange: [""]
    });
    this.fourthFormGroup = this._formBuilder.group({
      priority: [""]
    });
  }


  setName() {
    this.user.name = this.firstFormGroup.value.name?this.firstFormGroup.value.name:"";
  }
  setEmail() {
    this.user.emailId = this.secondFormGroup.value.EmailId?this.secondFormGroup.value.EmailId:"";
  }

  matChipSelected(chip: MatChipInputEvent,index:number) {
    /* if(chip.selected == true && this.totalSelected<2) {
      this.totalSelected++;
    } else if (chip.selected == true) {
      chip.selected = false;
    } else if(chip.selected==false && this.totalSelected>0) {
      this.totalSelected--;
    } */
    /* if(this.totalSelected<3 && state==true) {
      e.selected = false
      this.totalSelected++;
    }
    if(this.totalSelected>0 && state == false) {
      this.totalSelected--;
    } */
    /* if(chip.selected == false && this.totalSelected<2) {
      chip.color='warn';
      chip.selected=true;
      this.totalSelected++;
    }
    if(chip.selected == true) {
      chip.color = 'accent';
      chip.selected=false;
      this.totalSelected--;
    } */
    if(this.totalSelected>=3) {

    } else {

    }
    if(this.Priority.indexOf(index)!=-1) {
      const tempIndex = this.Priority.indexOf(index, 0);
      if (index > -1) {
            this.Priority.splice(tempIndex, 1);
        }
    } else {
      this.Priority.push(index);
    }
    this.totalSelected=this.Priority.length;
    console.log(this.Priority);
  }

  matclick(item:MatChip,index:number) {
    if(item.selected) {
      item.selected=false;
      const tempIndex = this.Priority.indexOf(index, 0);
      if (index > -1) {
            this.Priority.splice(tempIndex, 1);
      }
    } else {
      if(this.totalSelected>=3) {
        item.selected=false;
    } else {
        item.selected=true;
        this.Priority.push(index);
    }
    }
    this.totalSelected=this.Priority.length;
    
  }
  submit() {
    if(this.totalSelected>0) {
      this.recommendationService.setPriority(this.Priority);
      console.log(this.Priority);
      this.recommendationService.setUser(this.user);
      this.recommendationService.setPriceRange(this.value,this.maxValue);
      this.route.navigate(["display-results"]);
    }
  }

}
