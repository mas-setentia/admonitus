import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneResultComponent } from './phone-result.component';

describe('PhoneResultComponent', () => {
  let component: PhoneResultComponent;
  let fixture: ComponentFixture<PhoneResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
