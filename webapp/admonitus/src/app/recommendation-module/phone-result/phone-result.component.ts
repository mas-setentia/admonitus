import { Component, OnInit } from '@angular/core';
import { Phone } from '../Phone';
import { RecommendationServiceService } from '../recommendation-service.service';
import { User } from '../User';

@Component({
  selector: 'app-phone-result',
  templateUrl: './phone-result.component.html',
  styleUrls: ['./phone-result.component.css']
})
export class PhoneResultComponent implements OnInit {

  phoneList: Array<Phone> = [];
  user:User = new User();
  constructor(private recommendationService:RecommendationServiceService) { }

  ngOnInit() {
    this.recommendationService.getPhoneRecommendation().subscribe((res:any) => {
      this.phoneList = res;
      console.log(res);
    })
    // this.phoneList = this.recommendationService.getPhoneRecommendation();
    this.user = this.recommendationService.getUser();
  }
}
