import { Injectable } from '@angular/core';
import { EndpointService } from '../Endpoint-service/endpoint.service';
import { Constants } from '../Models/Constants';
import { Phone } from './Phone';
import { User } from './User';

@Injectable({
  providedIn: 'root'
})
export class RecommendationServiceService {

  constructor(private endpointService: EndpointService) {
    this.defaultPriority.push(Constants.PERFORMANCE_ENUM);
    this.defaultPriority.push(Constants.BATTERY_ENUM);
    this.defaultPriority.push(Constants.FRONT_CAMERA_ENUM);
    this.defaultPriority.push(Constants.REAR_CAMERA_ENUM);
    this.defaultPriority.push(Constants.BUILD_ENUM);
    this.defaultPriority.push(Constants.SOFTWARE_ENUM);
    this.defaultPriority.push(Constants.RAM_SCORE_ENUM);
    this.defaultPriority.push(Constants.DESIGN_ENUM);
    this.defaultPriority.push(Constants.DISPLAY_ENUM);
    this.defaultPriority.push(Constants.CHARGING_ENUM);
    this.defaultPriority.push(Constants.STORAGE_ENUM);
    this.defaultPriority.push(Constants.REFRESH_RATE_ENUM);
  }
  private defaultPriority:Array<string> = [];
  private requestPriority: Array<string> = [];
  private user: User = new User();
  private PhoneList: Array<Phone> = [];
  private value:number=0;
  private maxValue:number=0;
  private priceLow:number=0;
  private priceHigh:number=0;

  public getPhoneList() {
    this.endpointService.getPhoneList().subscribe((res: any) => {
      this.PhoneList = res;
    }, (error) => {
      console.log(error);
    }
    )
    return this.PhoneList;
  }
  public setPriority(priority: Array<number>) {
    this.requestPriority = [];
    priority.forEach(temp => {
      this.requestPriority.push(this.defaultPriority[temp]);
    })
  }
  public setUser(user: User) {
    this.user = user;
  }
  public getUser() {
    return this.user;
  }
  public setPriceRange(priceLow:number,priceHigh:number) {
    this.priceLow = priceLow;
    this.priceHigh = priceHigh;
  }

  public getPhoneRecommendation() {
    return this.endpointService.getPhoneRecommendation(this.priceLow ,this.priceHigh,this.requestPriority);
  }
}
