import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './site/landing-page/landing-page.component';
import { LoginComponent } from './login/login/login.component';
import { RecommendationFormComponent } from './recommendation-module/recommendation-form/recommendation-form.component';
import { AdminLandingComponent } from './admin/admin-landing/admin-landing.component';
import { AuthGuard } from './auth.guard';
import { PhoneResultComponent } from './recommendation-module/phone-result/phone-result.component';
import { AddScoreComponent } from './admin/add-score/add-score.component';
import { AddManufacturerComponent } from './admin/add-manufacturer/add-manufacturer.component';


const routes: Routes = [{path:"",component:LandingPageComponent},
{
  path:"login",component: LoginComponent
},{
  path: "recommendation-start",component: RecommendationFormComponent
},{
  path:"admin-landing",component:AdminLandingComponent,canActivate:[AuthGuard]
},{
  path:"display-results",component:PhoneResultComponent
},{
  path:"add-score",component: AddScoreComponent,canActivate:[AuthGuard]
}, {
  path:"add-manufacturer",component: AddManufacturerComponent,canActivate:[AuthGuard]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
