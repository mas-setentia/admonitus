export abstract class Endpoints {
    static readonly phoneListEndpoint:string="http://localhost:8080/temp/getphoneList";
    static readonly addPhoneScore:string="http://localhost:8080/add-score";
    static readonly getManufacturer:string="http://localhost:8080/manufacturers";
    static readonly getDisplayName:string = "http://localhost:8080/temp/display-phone-model";
    static readonly getFilterPhoneList:string = "http://localhost:8080/temp/getphones-filter";
    static readonly savePhoneModel:string = "http://localhost:8080/save-phone-model";
    static readonly deletePhoneEndpoint:string="http://localhost:8080/temp/deletePhoneModel";
}