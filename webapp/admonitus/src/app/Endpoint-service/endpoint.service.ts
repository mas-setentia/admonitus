import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { url } from 'inspector';
import { PhoneScore } from '../Models/PhoneScore';
import { SavePhoneModel } from '../Models/SavePhone';
import { Endpoints } from './Endpoints';

@Injectable({
  providedIn: 'root'
})
export class EndpointService {

  constructor(private httpClient: HttpClient) { }

  getPhoneList() {
    return this.httpClient.get(Endpoints.phoneListEndpoint);
  }

  addPhoneScore(phoneScore:PhoneScore) {
    return this.httpClient.post(Endpoints.addPhoneScore,phoneScore);
  }

  getManufacturers() {
    return this.httpClient.get(Endpoints.getManufacturer);
  }

  getPhoneModel(manufacturer:string) {
    return this.httpClient.get(Endpoints.getDisplayName,{params: {manufacturerName:manufacturer}});
  }

  getPhoneRecommendation(priceLow:number,priceHigh:number,priority:string[]) {
    return this.httpClient.post(Endpoints.getFilterPhoneList,priority,{
      params: {
        priceLow:priceLow+'',
        priceHigh:priceHigh+''
      }
    })
    // return this.httpClient.post(Endpoints.getFilterPhoneList,priority,{
    //   params:{
    //     priceLow:priceLow.toString(),
    //     priceHigh:priceHigh.toString()
    //   }
    // });
  }
  addPhoneModel(savePhone:SavePhoneModel) {
    return this.httpClient.post(Endpoints.savePhoneModel,savePhone);
  }

  deletePhones(modelName:String){
    return this.httpClient.delete(Endpoints.deletePhoneEndpoint,{params:{
      modelName:modelName+''
    }})
  }
}
