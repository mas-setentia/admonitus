import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { from } from 'rxjs';
import { AuthenticationService } from 'src/app/site/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  error: string;
  isLoggedIn: boolean;
  loginStatus: boolean;
  movieId: number;
  invalidData: boolean =false;

  constructor(private router: ActivatedRoute,private route: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.movieId = 0;
    this.loginStatus = true;
    this.isLoggedIn = true;
    this.loginForm = new FormGroup({
      'emailId': new FormControl('', [Validators.required,Validators.email]
      ),
      'passwrd': new FormControl('', [Validators.required]
      )
    });
  }
  onSubmit(loginForm) {
    if(loginForm.value.emailId === "admin@gmail.com" && loginForm.value.passwrd ==="admin") {
      this.authenticationService.setRole("ADMIN");
      this.route.navigate(["admin-landing"]);
    }
  }

}
