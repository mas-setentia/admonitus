import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private role:String="anonymous";
  constructor() { }

  getRole() {
    return this.role;
  }
  setRole(role:String) {
    this.role = role;
  }
}
