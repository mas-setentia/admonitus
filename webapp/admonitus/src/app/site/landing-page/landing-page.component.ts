import { Component, OnInit,Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RecommendationFormComponent } from 'src/app/recommendation-module/recommendation-form/recommendation-form.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(public dialog:MatDialog,private router: Router) { }

  ngOnInit() {
  }

  openModal() {
    this.router.navigate(['/recommendation-start']);
  }

}
