import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { element } from 'protractor';
import { Manufacturer } from 'src/app/Models/Manufacturer';

import { EndpointService } from 'src/app/Endpoint-service/endpoint.service';
import { Battery } from 'src/app/Models/BatteryScore';
import { Build } from 'src/app/Models/BuildScore';
import { Camera } from 'src/app/Models/CameraScore';
import { Charger } from 'src/app/Models/ChargerScore';
import { Design } from 'src/app/Models/DesignScore';
import { Display } from 'src/app/Models/DisplayScore';
import { FrontCamera } from 'src/app/Models/FrontCameraScore';
import { InHouseRating } from 'src/app/Models/InHouseScore';
import { Performance } from 'src/app/Models/PerformanceScore';
import { ModelInfromation } from 'src/app/Models/ModelInformation';
import { PhoneScore } from 'src/app/Models/PhoneScore';
import { RAM } from 'src/app/Models/RamScore';
import { RearCamera } from 'src/app/Models/RearCameraScore';
import { RefreshRate } from 'src/app/Models/RefreshRateScore';
import { Retailer } from 'src/app/Models/RetailerScore';
import { Software } from 'src/app/Models/SoftwareScore';
import {Storage} from 'src/app/Models/StorageScore';

@Component({
  selector: 'app-add-score',
  templateUrl: './add-score.component.html',
  styleUrls: ['./add-score.component.css']
})
export class AddScoreComponent implements OnInit {

  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;
  seventhFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  ninethFormGroup: FormGroup;
  tenthFormGroup: FormGroup;
  eleventhFormGroup: FormGroup;
  twelvethFormGroup: FormGroup;
  thirteenthFormGroup: FormGroup;
  fourteenthFormGroup: FormGroup;
  fifteenthFormGroup: FormGroup;
  sixteenthFormGroup: FormGroup;
  phoneScore: PhoneScore;
  apiLoadingScreen: boolean = false;
  manufacturers: Manufacturer[] = [];
  phoneModelMap: any[] = [];

  constructor(private _formBuilder: FormBuilder, private endPointService: EndpointService) { }

  ngOnInit() {
    this.endPointService.getManufacturers().subscribe((res: any) => {
      console.log(res);
      this.manufacturers = res;

      this.hideloader();
    });

    this.phoneScore = new PhoneScore();
    this.firstFormGroup = this._formBuilder.group({
      modelName: [null, [Validators.required]],
      brandName: [null, [Validators.required]],
      price: [0, [Validators.required]]
    });

    this.secondFormGroup = this._formBuilder.group({
      specURL: [null, [Validators.required]],
      officialURL: [null, [Validators.required]]
    });

    this.thirdFormGroup = this._formBuilder.group({
      backPanel: [0, [Validators.required]],
      backPanelProtection: [0, [Validators.required]],
      frame: [0, [Validators.required]],
      thickness: [0, [Validators.required]],
      weight: [0, [Validators.required]],
      waterResistance: [0, [Validators.required]],
      buildQuality: [0, [Validators.required]],
      buildQualityTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });
    this.fourthFormGroup = this._formBuilder.group({
      backPanelFinishing: [0, [Validators.required]],
      simSlot: [0, [Validators.required]],
      headphoneJack: [0, [Validators.required]],
      speakers: [0, [Validators.required]],
      additionalFeatures: [0, [Validators.required]],
      designTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });
    this.fifthFormGroup = this._formBuilder.group({
      displaySize: [0, [Validators.required]],
      displayResolution: [0, [Validators.required]],
      bezels: [0, [Validators.required]],
      displayType: [0, [Validators.required]],
      displayProtection: [0, [Validators.required]],
      displayPeakBrightness: [0, [Validators.required]],
      hdrSupport: [0, [Validators.required]],
      pixelDensity: [0, [Validators.required]],
      displayTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });
    this.sixthFormGroup = this._formBuilder.group({
      displayRefreshRate: [0, [Validators.required]],
      touchSamplingRate: [0, [Validators.required]],
      additionalDisplayFeatures: [0, [Validators.required]],
      refreshRateTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });

    this.seventhFormGroup = this._formBuilder.group({
      processor: [0, [Validators.required]],
      coreCount: [0, [Validators.required]],
      cpuClockSpeed: [0, [Validators.required]],
      gpu: [0, [Validators.required]],
      networkSim: [0, [Validators.required]],
      networkWifi: [0, [Validators.required]],
      networkBluetooth: [0, [Validators.required]],
      securityBiometric: [0, [Validators.required]],
      performanceTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });

    this.eighthFormGroup = this._formBuilder.group({
      ramCapacity: [0, [Validators.required]],
      ramTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });

    this.ninethFormGroup = this._formBuilder.group({
      storageCapacity: [0, [Validators.required]],
      expandableStorage: [0, [Validators.required]],
      storageTotal: [{ value: 0, disabled: true }, [Validators.required]]
    })

    this.tenthFormGroup = this._formBuilder.group({
      rearPCAperture: [0, [Validators.required]],
      rearPCSensorSize: [0, [Validators.required]],
      rearPCImageStablization: [0, [Validators.required]],
      rearPCResolution: [0, [Validators.required]],
      rearSecondCameraType: [0, [Validators.required]],
      rearSecondCameraResolution: [0, [Validators.required]],
      rearSecondCameraAperture: [0, [Validators.required]],
      rearSecondCameraSensorSize: [0, [Validators.required]],
      rearSecondCameraImageStablization: [0, [Validators.required]],
      rearSecondCameraAdditionalFeatures: [0, [Validators.required]],
      rearThirdCameraType: [0, [Validators.required]],
      rearThirdCameraResolution: [0, [Validators.required]],
      rearThirdCameraAperture: [0, [Validators.required]],
      rearThirdCameraSensorSize: [0, [Validators.required]],
      rearThirdCameraImageStablization: [0, [Validators.required]],
      rearThirdCameraAdditionalFeatures: [0, [Validators.required]],
      rearOtherCameraFeatures: [0, [Validators.required]],
      rearImageProcessingPhoto: [0, [Validators.required]],
      rearVideoCapablity: [0, [Validators.required]],
      rearSlowMotionVideoCapablity: [0, [Validators.required]],
      rearSpecialFeatures: [0, [Validators.required]],
      rearNightMode: [0, [Validators.required]],
      rearImageProcessingVideo: [0, [Validators.required]],
      rearCameraTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });
    this.eleventhFormGroup = this._formBuilder.group({
      frontPCAperture: [0, [Validators.required]],
      frontPCSensorSize: [0, [Validators.required]],
      frontPCResolution: [0, [Validators.required]],
      frontPCImageStablization: [0, [Validators.required]],
      frontSecondCameraType: [0, [Validators.required]],
      frontSecondCameraResolution: [0, [Validators.required]],
      frontSecondCameraAperture: [0, [Validators.required]],
      frontSecondCameraSensorSize: [0, [Validators.required]],
      frontSecondCameraImageStablization: [0, [Validators.required]],
      frontSecondCameraAdditionalFeatures: [0, [Validators.required]],
      frontOtherCameraFeatures: [0, [Validators.required]],
      frontImageProcessingPhoto: [0, [Validators.required]],
      frontVideoCapablity: [0, [Validators.required]],
      frontSpecialFeatures: [0, [Validators.required]],
      frontImageProcessingVideo: [0, [Validators.required]],
      frontCameraTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });

    this.twelvethFormGroup = this._formBuilder.group({
      batteryCapacity: [0, [Validators.required]],
      chargingInterface: [0, [Validators.required]],
      batteryOptimization: [0, [Validators.required]],
      batteryTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });

    this.thirteenthFormGroup = this._formBuilder.group({
      chargerRating: [0, [Validators.required]],
      wirelessChargingSupport: [0, [Validators.required]],
      reverseCharging: [0, [Validators.required]],
      additionalFeatures: [0, [Validators.required]],
      chargingSpeedTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });

    this.fourteenthFormGroup = this._formBuilder.group({
      osVersion: [0, [Validators.required]],
      userInterface: [0, [Validators.required]],
      softwareTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });

    this.fifteenthFormGroup = this._formBuilder.group({
      customerSupport: [0, [Validators.required]],
      serviceSupport: [0, [Validators.required]],
      innovation: [0, [Validators.required]],
      editorRating: [0, [Validators.required]],
      criticRating: [0, [Validators.required]],
      thirdPartyScore: [0, [Validators.required]],
      exclusiveFactor: [0, [Validators.required]],
      inhouseTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });

    this.sixteenthFormGroup = this._formBuilder.group({
      demoUnit: [0, [Validators.required]],
      margin: [0, [Validators.required]],
      brandPromoter: [0, [Validators.required]],
      schemes: [0, [Validators.required]],
      extraCredit: [0, [Validators.required]],
      retailerPromotionTotal: [{ value: 0, disabled: true }, [Validators.required]]
    });
  }
  onNext() {

  }
  calculateTotal(formGroup: FormGroup) {
    var total = 0
    for (const temp in formGroup.controls) {
      var value = formGroup.get(temp).value
      if (value != 0) {
        total += value;
      }
    }
    return total
  }
  onSubmit() {
    this.showLoader();
    this.phoneScore.modelInformation = new ModelInfromation();
    this.phoneScore.modelInformation.brandName = this.firstFormGroup.controls['brandName'].value;
    this.phoneScore.modelInformation.modelName = this.firstFormGroup.controls['modelName'].value;
    this.phoneScore.modelInformation.price = this.firstFormGroup.controls['price'].value;

    this.phoneScore.modelInformation.specURL = this.secondFormGroup.controls['specURL'].value;
    this.phoneScore.modelInformation.officialURL = this.secondFormGroup.controls['officialURL'].value;

    this.phoneScore.buildInformation = new Build();
    this.phoneScore.buildInformation.buildBackPanel = this.thirdFormGroup.controls['backPanel'].value;
    this.phoneScore.buildInformation.buildBackPanelProtection = this.thirdFormGroup.controls['backPanelProtection'].value;
    this.phoneScore.buildInformation.buildFrame = this.thirdFormGroup.controls['frame'].value;
    this.phoneScore.buildInformation.buildThickness = this.thirdFormGroup.controls['thickness'].value;
    this.phoneScore.buildInformation.buildWaterResistance = this.thirdFormGroup.controls['waterResistance'].value;
    this.phoneScore.buildInformation.buildWeight = this.thirdFormGroup.controls['weight'].value;
    this.phoneScore.buildInformation.buildQuality = this.thirdFormGroup.controls['buildQuality'].value;
    this.phoneScore.buildInformation.buildTotal = this.thirdFormGroup.controls['buildQualityTotal'].value;

    this.phoneScore.designInformation = new Design();
    this.phoneScore.designInformation.designBackPanelFinishing = this.fourthFormGroup.controls['backPanelFinishing'].value;
    this.phoneScore.designInformation.designHeadphoneJack = this.fourthFormGroup.controls['headphoneJack'].value;
    this.phoneScore.designInformation.designSimSlot = this.fourthFormGroup.controls['simSlot'].value;
    this.phoneScore.designInformation.speakers = this.fourthFormGroup.controls['speakers'].value;
    this.phoneScore.designInformation.designAdditionalFeatures = this.fourthFormGroup.controls['additionalFeatures'].value;
    this.phoneScore.designInformation.designTotal = this.fourthFormGroup.controls['designTotal'].value;

    this.phoneScore.displayInformation = new Display();
    this.phoneScore.displayInformation.displayBezels = this.fifthFormGroup.controls['bezels'].value;
    this.phoneScore.displayInformation.displayHdrSupport = this.fifthFormGroup.controls['hdrSupport'].value;
    this.phoneScore.displayInformation.displayPeakBrightness = this.fifthFormGroup.controls['displayPeakBrightness'].value;
    this.phoneScore.displayInformation.displayPixelDensity = this.fifthFormGroup.controls['pixelDensity'].value;
    this.phoneScore.displayInformation.displayProtection = this.fifthFormGroup.controls['displayProtection'].value;
    this.phoneScore.displayInformation.displayResolution = this.fifthFormGroup.controls['displayResolution'].value;
    this.phoneScore.displayInformation.displaySize = this.fifthFormGroup.controls['displaySize'].value;
    this.phoneScore.displayInformation.displayType = this.fifthFormGroup.controls['displayType'].value;
    this.phoneScore.displayInformation.displayTotal = this.fifthFormGroup.controls['displayTotal'].value;

    this.phoneScore.refreshRateInformation = new RefreshRate();
    this.phoneScore.refreshRateInformation.refreshRate = this.sixthFormGroup.controls['displayRefreshRate'].value;
    this.phoneScore.refreshRateInformation.touchSamplingRate = this.sixthFormGroup.controls['touchSamplingRate'].value;
    this.phoneScore.refreshRateInformation.additionalDisplayFeatures = this.sixthFormGroup.controls['additionalDisplayFeatures'].value;
    this.phoneScore.refreshRateInformation.displayRefreshRateTotal = this.sixthFormGroup.controls['refreshRateTotal'].value;

    this.phoneScore.performanceInformation = new Performance();
    this.phoneScore.performanceInformation.coreCount = this.seventhFormGroup.controls['coreCount'].value;
    this.phoneScore.performanceInformation.cpuClockSpeed = this.seventhFormGroup.controls['cpuClockSpeed'].value;
    this.phoneScore.performanceInformation.gpu = this.seventhFormGroup.controls['gpu'].value;
    this.phoneScore.performanceInformation.networkBluetooth = this.seventhFormGroup.controls['networkBluetooth'].value;
    this.phoneScore.performanceInformation.networkSim = this.seventhFormGroup.controls['networkSim'].value;
    this.phoneScore.performanceInformation.networkWifi = this.seventhFormGroup.controls['networkWifi'].value;
    this.phoneScore.performanceInformation.processor = this.seventhFormGroup.controls['processor'].value;
    this.phoneScore.performanceInformation.securityBioMetric = this.seventhFormGroup.controls['securityBiometric'].value;
    this.phoneScore.performanceInformation.performanceTotal = this.seventhFormGroup.controls['performanceTotal'].value;

    this.phoneScore.ramInformation = new RAM();
    this.phoneScore.ramInformation.ramCapacity = this.eighthFormGroup.controls['ramCapacity'].value;
    this.phoneScore.ramInformation.ramTotal = this.eighthFormGroup.controls['ramTotal'].value;

    this.phoneScore.storageInformation = new Storage();
    this.phoneScore.storageInformation.storageCapacity = this.ninethFormGroup.controls['storageCapacity'].value;
    this.phoneScore.storageInformation.expandableStorage = this.ninethFormGroup.controls['expandableStorage'].value;
    this.phoneScore.storageInformation.storageTotal = this.ninethFormGroup.controls['storageTotal'].value;

    var camera1: Camera = new Camera();
    var camera2: Camera = new Camera();
    var camera3: Camera = new Camera();
    var camera4: Camera = new Camera();
    var camera5: Camera = new Camera();

    camera1.cameraAperture = this.tenthFormGroup.controls['rearPCAperture'].value;
    camera1.cameraResolution = this.tenthFormGroup.controls['rearPCSensorSize'].value;
    camera1.cameraImageStablization = this.tenthFormGroup.controls['rearPCImageStablization'].value;
    camera1.cameraSensorSize = this.tenthFormGroup.controls['rearPCResolution'].value;

    camera2.cameraType = this.tenthFormGroup.controls['rearSecondCameraType'].value;
    camera2.cameraAperture = this.tenthFormGroup.controls['rearSecondCameraAperture'].value;
    camera2.cameraResolution = this.tenthFormGroup.controls['rearSecondCameraResolution'].value;
    camera2.cameraImageStablization = this.tenthFormGroup.controls['rearSecondCameraImageStablization'].value;
    camera2.cameraSensorSize = this.tenthFormGroup.controls['rearSecondCameraAdditionalFeatures'].value;

    camera3.cameraType = this.tenthFormGroup.controls['rearThirdCameraType'].value;
    camera3.cameraAperture = this.tenthFormGroup.controls['rearThirdCameraAperture'].value;
    camera3.cameraResolution = this.tenthFormGroup.controls['rearThirdCameraResolution'].value;
    camera3.cameraImageStablization = this.tenthFormGroup.controls['rearThirdCameraImageStablization'].value;
    camera3.cameraSensorSize = this.tenthFormGroup.controls['rearThirdCameraSensorSize'].value;

    this.phoneScore.rearCameraInformation = new RearCamera();
    this.phoneScore.rearCameraInformation.rearCamera = [];
    this.phoneScore.rearCameraInformation.rearCamera.push(camera1);
    this.phoneScore.rearCameraInformation.rearCamera.push(camera2);
    this.phoneScore.rearCameraInformation.rearCamera.push(camera3);
    this.phoneScore.rearCameraInformation.rearImageProcessingPhoto = this.tenthFormGroup.controls['rearImageProcessingPhoto'].value;
    this.phoneScore.rearCameraInformation.rearImageProcessingVideo = this.tenthFormGroup.controls['rearImageProcessingVideo'].value;
    this.phoneScore.rearCameraInformation.rearNightMode = this.tenthFormGroup.controls['rearNightMode'].value;
    this.phoneScore.rearCameraInformation.rearOtherCameraFeatures = this.tenthFormGroup.controls['rearOtherCameraFeatures'].value;
    this.phoneScore.rearCameraInformation.rearSlowMotionVideoCapablity = this.tenthFormGroup.controls['rearSlowMotionVideoCapablity'].value;
    this.phoneScore.rearCameraInformation.rearSpecialFeatures = this.tenthFormGroup.controls['rearSpecialFeatures'].value;
    this.phoneScore.rearCameraInformation.rearVideoCapablity = this.tenthFormGroup.controls['rearVideoCapablity'].value;
    this.phoneScore.rearCameraInformation.rearCameraTotal = this.tenthFormGroup.controls['rearCameraTotal'].value;

    camera4.cameraType = this.eleventhFormGroup.controls['frontSecondCameraType'].value;
    camera4.cameraAperture = this.eleventhFormGroup.controls['frontPCAperture'].value;
    camera4.cameraResolution = this.eleventhFormGroup.controls['frontPCResolution'].value;
    camera4.cameraImageStablization = this.eleventhFormGroup.controls['frontPCImageStablization'].value;
    camera4.cameraSensorSize = this.eleventhFormGroup.controls['frontPCSensorSize'].value;

    camera5.cameraType = this.eleventhFormGroup.controls['frontSecondCameraType'].value;
    camera5.cameraAperture = this.eleventhFormGroup.controls['frontSecondCameraAperture'].value;
    camera5.cameraResolution = this.eleventhFormGroup.controls['frontSecondCameraResolution'].value;
    camera5.cameraImageStablization = this.eleventhFormGroup.controls['frontSecondCameraImageStablization'].value;
    camera5.cameraSensorSize = this.eleventhFormGroup.controls['frontSecondCameraSensorSize'].value;
    camera5.cameraAdditionalFeatures = this.eleventhFormGroup.controls['frontSecondCameraAdditionalFeatures'].value;

    this.phoneScore.frontCameraInformation = new FrontCamera();
    this.phoneScore.frontCameraInformation.frontCamera = [];
    this.phoneScore.frontCameraInformation.frontCamera.push(camera4);
    this.phoneScore.frontCameraInformation.frontCamera.push(camera5);

    this.phoneScore.frontCameraInformation.frontImageProcessingPhoto = this.eleventhFormGroup.controls['frontImageProcessingPhoto'].value;
    this.phoneScore.frontCameraInformation.frontImageProcessingVideo = this.eleventhFormGroup.controls['frontImageProcessingVideo'].value;
    this.phoneScore.frontCameraInformation.frontSpecialFeatures = this.eleventhFormGroup.controls['frontSpecialFeatures'].value;
    this.phoneScore.frontCameraInformation.frontOtherCameraFeatures = this.eleventhFormGroup.controls['frontOtherCameraFeatures'].value;
    this.phoneScore.frontCameraInformation.frontVideoCapablity = this.eleventhFormGroup.controls['frontVideoCapablity'].value;
    this.phoneScore.frontCameraInformation.frontCameraTotal = this.eleventhFormGroup.controls['frontCameraTotal'].value;

    this.phoneScore.batteryInformation = new Battery();
    this.phoneScore.batteryInformation.batteryCapacity = this.twelvethFormGroup.controls['batteryCapacity'].value;
    this.phoneScore.batteryInformation.batteryChargingInterface = this.twelvethFormGroup.controls['chargingInterface'].value;
    this.phoneScore.batteryInformation.batteryOptimization = this.twelvethFormGroup.controls['batteryOptimization'].value;
    this.phoneScore.batteryInformation.batteryTotal = this.twelvethFormGroup.controls['batteryTotal'].value;

    this.phoneScore.chargerInformation = new Charger();
    this.phoneScore.chargerInformation.chargerRating = this.thirteenthFormGroup.controls['chargerRating'].value;
    this.phoneScore.chargerInformation.reverseCharging = this.thirteenthFormGroup.controls['reverseCharging'].value;
    this.phoneScore.chargerInformation.wirelessChargingSupport = this.thirteenthFormGroup.controls['wirelessChargingSupport'].value;
    this.phoneScore.chargerInformation.chargerAdditionalFeatures = this.thirteenthFormGroup.controls['additionalFeatures'].value;
    this.phoneScore.chargerInformation.chargerTotal = this.thirteenthFormGroup.controls['chargingSpeedTotal'].value;

    this.phoneScore.softwareInformation = new Software();
    this.phoneScore.softwareInformation.softwareOS = this.fourteenthFormGroup.controls['osVersion'].value;
    this.phoneScore.softwareInformation.softwareUserInterface = this.fourteenthFormGroup.controls['userInterface'].value;
    this.phoneScore.softwareInformation.softwareTotal = this.fourteenthFormGroup.controls['softwareTotal'].value;

    this.phoneScore.inHouseInformation = new InHouseRating();
    this.phoneScore.inHouseInformation.criticRating = this.fifteenthFormGroup.controls['criticRating'].value;
    this.phoneScore.inHouseInformation.customerSupport = this.fifteenthFormGroup.controls['customerSupport'].value;
    this.phoneScore.inHouseInformation.editorRating = this.fifteenthFormGroup.controls['editorRating'].value;
    this.phoneScore.inHouseInformation.exclusiveFactor = this.fifteenthFormGroup.controls['exclusiveFactor'].value;
    this.phoneScore.inHouseInformation.innovation = this.fifteenthFormGroup.controls['innovation'].value;
    this.phoneScore.inHouseInformation.serviceSupport = this.fifteenthFormGroup.controls['serviceSupport'].value;
    this.phoneScore.inHouseInformation.thirdPartyScore = this.fifteenthFormGroup.controls['thirdPartyScore'].value;
    this.phoneScore.inHouseInformation.inHouseTotal = this.fifteenthFormGroup.controls['inhouseTotal'].value;

    this.phoneScore.retailerInformation = new Retailer();
    this.phoneScore.retailerInformation.brandPromoter = this.sixteenthFormGroup.controls['brandPromoter'].value;
    this.phoneScore.retailerInformation.demoUnit = this.sixteenthFormGroup.controls['demoUnit'].value;
    this.phoneScore.retailerInformation.extraCredit = this.sixteenthFormGroup.controls['extraCredit'].value;
    this.phoneScore.retailerInformation.margin = this.sixteenthFormGroup.controls['margin'].value;
    this.phoneScore.retailerInformation.schemes = this.sixteenthFormGroup.controls['schemes'].value;
    this.phoneScore.retailerInformation.retailerTotal = this.sixteenthFormGroup.controls['retailerPromotionTotal'].value;

    console.log(this.phoneScore);
    this.endPointService.addPhoneScore(this.phoneScore).subscribe((res) => {
      console.log(res);
      this.hideloader();
    });

  }
  hideloader() {
    this.apiLoadingScreen = false;
    // // Setting display of spinner 
    // // element to none 
    // document.getElementById('loading') 
    //     .style.display = 'none !important'; 
  }
  showLoader() {
    // document.getElementById('loading') 
    //     .style.display = 'flex !important';
    this.apiLoadingScreen = true;
  }

  getPhoneModel(manufacturer: any) {
    console.log(manufacturer)
    this.endPointService.getPhoneModel(manufacturer.name).subscribe((res: any) => {
      console.log(res);
      this.phoneModelMap = res;
    })
  }
}
