import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-manufacturer',
  templateUrl: './add-manufacturer.component.html',
  styleUrls: ['./add-manufacturer.component.css']
})
export class AddManufacturerComponent implements OnInit {

  manufacturerFormGroup: FormGroup;
  constructor(private _formBuilder:FormBuilder) { }

  ngOnInit() {
    this.manufacturerFormGroup = this._formBuilder.group({
      manufacturer_name:[null,[Validators.required]],
    manufacturer_display_name:[null,[Validators.required]],
    manufacturer_parent_company:[null,[Validators.required]],
    manufacturer_origin:[null,[Validators.required]]
    })
  }

}
