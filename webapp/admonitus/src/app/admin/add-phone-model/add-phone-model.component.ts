import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Manufacturer } from 'src/app/Models/Manufacturer';
import { EndpointService } from 'src/app/Endpoint-service/endpoint.service';
import { SavePhoneModel } from 'src/app/Models/SavePhone';

@Component({
  selector: 'app-add-phone-model',
  templateUrl: './add-phone-model.component.html',
  styleUrls: ['./add-phone-model.component.css']
})
export class AddPhoneModelComponent implements OnInit {

  constructor(private endPointService: EndpointService,private _formBuilder:FormBuilder) { }

  manufacturers:Manufacturer[] = [];
  apiLoadingScreen:boolean = false;
  firstFormGroup: FormGroup;
  selectedManufacturer:Manufacturer = new Manufacturer();

  ngOnInit() {
    this.endPointService.getManufacturers().subscribe((res:any) => {
      console.log(res);
      this.manufacturers=res;
      this.hideloader();
    });
    this.firstFormGroup = this._formBuilder.group({
      modelName: [null,[Validators.required]],
      variantName: [null,[Validators.required]],
      manufacturer: [null,[Validators.required]]
    });
  }

  hideloader() { 
    this.apiLoadingScreen = false;
    // // Setting display of spinner 
    // // element to none 
    // document.getElementById('loading') 
    //     .style.display = 'none !important'; 
  }
  showLoader() {
    // document.getElementById('loading') 
    //     .style.display = 'flex !important';
    this.apiLoadingScreen = true; 
  }

  addPhoneModel() {
    var savePhone = new SavePhoneModel();
    savePhone.manufacturer = this.selectedManufacturer;
    savePhone.modelName = this.firstFormGroup.get('modelName').value;
    savePhone.variantName = this.firstFormGroup.get('variantName').value;
    this.endPointService.addPhoneModel(savePhone).subscribe((res:any)=> {
      console.log("Rest success");
    })

  }
  setManufacturer(manufacturer:Manufacturer) {
    this.selectedManufacturer = manufacturer;
  }

}
