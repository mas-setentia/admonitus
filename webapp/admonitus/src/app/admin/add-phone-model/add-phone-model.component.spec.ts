import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPhoneModelComponent } from './add-phone-model.component';

describe('AddPhoneModelComponent', () => {
  let component: AddPhoneModelComponent;
  let fixture: ComponentFixture<AddPhoneModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPhoneModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPhoneModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
