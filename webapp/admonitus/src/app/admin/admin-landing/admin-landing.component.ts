import { EventEmitter } from '@angular/core';
import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-admin-landing',
  templateUrl: './admin-landing.component.html',
  styleUrls: ['./admin-landing.component.css']
})
export class AdminLandingComponent implements OnInit {

  events: string[] = [];
  opened: boolean;
  selected:String='Add Score';

  shouldRun = true
  constructor() { }

  ngOnInit() {
  }

}
