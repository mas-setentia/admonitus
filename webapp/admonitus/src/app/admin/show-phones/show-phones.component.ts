import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/Endpoint-service/endpoint.service';
import { Phone } from 'src/app/recommendation-module/Phone';

@Component({
  selector: 'app-show-phones',
  templateUrl: './show-phones.component.html',
  styleUrls: ['./show-phones.component.css']
})
export class ShowPhonesComponent implements OnInit {

  private phoneList: Array<Phone> = [];

  constructor(private endpointService:EndpointService) { }

  ngOnInit() {

    this.endpointService.getPhoneList().subscribe((phones:any)=> {
      console.log(phones)
      this.phoneList = phones;
    });

  }

  deletePhones(modelName:String){
    this.endpointService.deletePhones(modelName).subscribe(((res:any)=>{
      this.endpointService.getPhoneList().subscribe((phones:any)=> {
        console.log(phones)
        this.phoneList = phones;
      });
    }))
  }

}
