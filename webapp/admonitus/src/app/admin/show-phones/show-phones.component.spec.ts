import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPhonesComponent } from './show-phones.component';

describe('ShowPhonesComponent', () => {
  let component: ShowPhonesComponent;
  let fixture: ComponentFixture<ShowPhonesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPhonesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPhonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
