import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './site/header/header.component';
import { FooterComponent } from './site/footer/footer.component';
import { LandingPageComponent } from './site/landing-page/landing-page.component';
import { LoginComponent } from './login/login/login.component';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {MatFormFieldModule,
        MatInputModule,
        MatStepperModule,
        MatButtonModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatChipsModule,
        MatSidenavModule,
        MatCheckboxModule,
        MatOptionModule, MatSelectModule, MatListModule, MatExpansionModule
      }
 from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { RecommendationFormComponent } from './recommendation-module/recommendation-form/recommendation-form.component' 
import {Ng5SliderModule} from 'ng5-slider';
import { AdminLandingComponent } from './admin/admin-landing/admin-landing.component';
import { PhoneResultComponent } from './recommendation-module/phone-result/phone-result.component';
import { PhoneResultUnitComponent } from './recommendation-module/phone-result-unit/phone-result-unit.component';
import { AddScoreComponent } from './admin/add-score/add-score.component';
import { AddManufacturerComponent } from './admin/add-manufacturer/add-manufacturer.component';
import { AddPhoneModelComponent } from './admin/add-phone-model/add-phone-model.component';
import { ShowPhonesComponent } from './admin/show-phones/show-phones.component'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LandingPageComponent,
    LoginComponent,
    RecommendationFormComponent,
    AdminLandingComponent,
    PhoneResultComponent,
    PhoneResultUnitComponent,
    AddScoreComponent,
    AddManufacturerComponent,
    AddPhoneModelComponent,
    ShowPhonesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatStepperModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    Ng5SliderModule,
    MatChipsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatOptionModule,
    MatSelectModule,
    MatListModule,
    HttpClientModule,
    MatExpansionModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [RecommendationFormComponent]
})
export class AppModule { }
