import { Camera } from './CameraScore';

export class FrontCamera {
    frontCamera: Camera[];
    frontOtherCameraFeatures:number;
    frontImageProcessingPhoto:number;
    frontVideoCapablity:number;
    frontSpecialFeatures:number;
    frontImageProcessingVideo:number;
    frontCameraTotal:number;
}