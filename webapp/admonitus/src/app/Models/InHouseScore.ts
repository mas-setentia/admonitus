export class InHouseRating {
    customerSupport:number;
    serviceSupport:number;
    innovation:number;
    editorRating:number;
    criticRating:number;
    thirdPartyScore:number;
    exclusiveFactor:number;
    inHouseTotal:number;
}