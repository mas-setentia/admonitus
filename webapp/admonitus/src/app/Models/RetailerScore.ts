export class Retailer {
    demoUnit:number;
    margin:number;
    brandPromoter:number;
    schemes:number;
    extraCredit:number;
    retailerTotal:number;
}