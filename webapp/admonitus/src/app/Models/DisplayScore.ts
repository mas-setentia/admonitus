export class Display {
    displaySize:number;
    displayResolution:number;
    displayBezels:number;
    displayType:number;
    displayProtection:number;
    displayPeakBrightness:number;
    displayHdrSupport:number;
    displayPixelDensity:number;
    displayTotal:number;
}