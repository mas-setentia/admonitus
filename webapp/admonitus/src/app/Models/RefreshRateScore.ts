export class RefreshRate {
    refreshRate:number;
    touchSamplingRate:number;
    additionalDisplayFeatures:number;
    displayRefreshRateTotal:number;
}