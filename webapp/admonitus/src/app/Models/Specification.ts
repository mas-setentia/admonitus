export class Specification {
    
    phoneModel:string;
	
	Camera:string;
	
	NetworkTechnology:string;
	
	announced:string;
	
	status:string;
	
	dimensions:string;
	
	weight:string;
	
	build:string;
	
	sim:string;
	
	bodyMeta:string[];
	
	displayType:string;
	
	displaySize:string;
	
	displayResolution:string;
	
	displayProtection:string;
	
	displayMeta:string[];
	
	softwareOS:string;
	
	chipset:string;
	
	CPU:string;
	
	GPU:string;
	
	softwareMeta:string[];
	
	memoyCard:string;
	
	memoryInternal:string;
	
	memoryMeta:string[];
	
	mainCameraList:string[];
	
	mainCameraFeatures:string;
	
	mainCameraVideo:string;
	
	mainCameraMeta:string[];
	
	frontCameraList:string;
	
	frontCameraFeatures:string;
	
	frontCameraVideo:string;
	
	frontCameraMeta:string;
	
	loudSpeaker:string;
	
	headPhoneJack:string;
	
	soundMeta:string;
	
	wlan:string;
	
	bluetooth:string;
	
	GPS:string;
	
	NFC:string;
	
	infraredPort:string;
	
	radio:string;
	
	usb:string;
	
	commsMeta:string;
	
	sensors:string;
	
	featuresMeta:string[];
	
	batteryType:string;
	
	charging:string;
	
	batteryMeta:string[];
	
	colors:string[];
}