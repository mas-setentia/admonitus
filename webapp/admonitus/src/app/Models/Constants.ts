export class Constants {
    
    public static BATTERY_ENUM = "BATTERY";
	
	public static BUILD_ENUM = "BUILD";
	
	public static CHARGING_ENUM = "CHARGING";
	
	public static DESIGN_ENUM = "DESIGN";
	
	public static DISPLAY_ENUM = "DISPLAY";
	
	public static FRONT_CAMERA_ENUM = "FRONT_CAMERA";
	
	public static IN_HOUSE_ENUM = "INHOUSE";
	
	public static PERFORMANCE_ENUM = "PERFORMANCE";
	
	public static RAM_SCORE_ENUM = "RAM";
	
	public static REAR_CAMERA_ENUM = "REAR_CAMERA";
	
	public static REFRESH_RATE_ENUM = "REFRESH_RATE";
	
	public static RETAILER_ENUM = "RETAILER";
	
	public static SOFTWARE_ENUM = "SOFTWARE";
	
	public static STORAGE_ENUM = "STORAGE";

}