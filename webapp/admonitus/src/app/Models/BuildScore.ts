export class Build {
    buildBackPanel:number;
    buildBackPanelProtection:number;
    buildFrame:number;
    buildThickness:number;
    buildWeight:number;
    buildWaterResistance:number;
    buildQuality:number;
    buildTotal:number;
}