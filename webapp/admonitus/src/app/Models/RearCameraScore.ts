import { Camera } from './CameraScore';

export class RearCamera {
    rearCamera: Camera[];
    rearOtherCameraFeatures:number;
    rearImageProcessingPhoto:number;
    rearVideoCapablity:number;
    rearSlowMotionVideoCapablity:number;
    rearSpecialFeatures:number;
    rearNightMode:number;
    rearImageProcessingVideo:number;
    rearCameraTotal:number;
}