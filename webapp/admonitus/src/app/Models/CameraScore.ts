export class Camera {
    cameraType:number;
    cameraResolution:number;
    cameraAperture:number;
    cameraSensorSize:number;
    cameraImageStablization:number;
    cameraAdditionalFeatures:number;
}