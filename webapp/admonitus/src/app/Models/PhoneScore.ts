import { Battery } from './BatteryScore';
import { Build } from './BuildScore';
import { Charger } from './ChargerScore';
import { Design } from './DesignScore';
import { Display } from './DisplayScore';
import { FrontCamera } from './FrontCameraScore';
import { InHouseRating } from './InHouseScore';
import { ModelInfromation } from './ModelInformation';
import { Performance } from './PerformanceScore';
import { RAM } from './RamScore';
import { RearCamera } from './RearCameraScore';
import { RefreshRate } from './RefreshRateScore';
import { Retailer } from './RetailerScore';
import { Software } from './SoftwareScore';
import { Storage } from './StorageScore';

export class PhoneScore {
    modelInformation:ModelInfromation;
    buildInformation: Build;
    designInformation: Design;
    displayInformation: Display;
    refreshRateInformation: RefreshRate
    performanceInformation: Performance;
    ramInformation: RAM;
    storageInformation: Storage;
    rearCameraInformation:RearCamera;
    frontCameraInformation:FrontCamera;
    batteryInformation:Battery;
    chargerInformation:Charger;
    softwareInformation: Software;
    inHouseInformation: InHouseRating;
    retailerInformation: Retailer;
}