export class Design {
    designBackPanelFinishing:number;
    designSimSlot:number;
    designHeadphoneJack:number;
    speakers:number;
    designAdditionalFeatures:number;
    designTotal:number;
}