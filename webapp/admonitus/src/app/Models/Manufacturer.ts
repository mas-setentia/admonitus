export class Manufacturer {
    manufacturer_name:string;
    manufacturer_display_name:string;
    manufacturer_parent_company:string;
    manufacturer_origin:string;
}