export class Charger {
    chargerRating:number;
    wirelessChargingSupport:number;
    reverseCharging:number;
    chargerAdditionalFeatures:number;
    chargerTotal:number;
}