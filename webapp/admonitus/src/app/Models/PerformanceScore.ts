export class Performance {
    processor:number;
    coreCount:number;
    cpuClockSpeed:number;
    gpu:number;
    networkSim:number;
    networkWifi:number;
    networkBluetooth:number;
    securityBioMetric:number;
    performanceTotal:number;
}