import { Manufacturer } from './Manufacturer';

export class SavePhoneModel {
    modelName:string;
    variantName:string;
    manufacturer:Manufacturer;
}