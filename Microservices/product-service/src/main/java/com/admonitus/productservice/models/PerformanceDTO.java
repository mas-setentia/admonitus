package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class PerformanceDTO {

	private int processor;
	
	private int coreCount;
	
	private int cpuClockSpeed;
	
	private int gpu;
	
	private int networkSim;
	
	private int networkWifi;
	
	private int networkBluetooth;
	
	private int securityBioMetric;
	
	private int performanceTotal;
}
