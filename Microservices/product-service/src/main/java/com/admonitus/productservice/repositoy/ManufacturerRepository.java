package com.admonitus.productservice.repositoy;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.admonitus.productservice.configuration.Manufacturer;

@Repository
public interface ManufacturerRepository extends MongoRepository<Manufacturer, ObjectId> {

	Optional<Manufacturer> findByDisplayName(String displayName);
}
