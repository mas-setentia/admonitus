package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class RAMDTO {

	private int ramCapacity;
	
	private int ramTotal;
}
