package com.admonitus.productservice.configuration;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class DisplayScore {

	@Id
	private long id;

	private int displayResolution;

	private int displaySize;

	private int displayBezels;

	private int displayType;

	private int displayProtection;

	private int displayPeakBrightness;

	private int displayHdrSupport;

	private int displayPixelDensity;

	private int displayTotal;

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

}
