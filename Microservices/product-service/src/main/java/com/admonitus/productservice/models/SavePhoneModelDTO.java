package com.admonitus.productservice.models;

import com.admonitus.productservice.configuration.Manufacturer;

import lombok.Data;

@Data
public class SavePhoneModelDTO {

	private String modelName;
	
	private String variantName;
	
	private Manufacturer manufacturer;
	
}
