package com.admonitus.productservice.models;

import java.util.List;

import lombok.Data;

@Data
public class RearCameraDTO {

	private List<CameraDTO> rearCamera;
	
	private int rearOtherCameraFeatures;
	
	private int rearImageProcessingPhoto;
	
	private int rearVideoCapablity;
	
	private int rearSlowMotionVideoCapablity;
	
	private int rearSpecialFeatures;
	
	private int rearNightMode;
	
	private int rearImageProcessingVideo;
	
	private int rearCameraTotal;
}
