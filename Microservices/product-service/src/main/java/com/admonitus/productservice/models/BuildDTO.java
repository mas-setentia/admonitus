package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class BuildDTO {

	private int buildBackPanel;
	
	private int buildBackPanelProtection;
	
	private int buildFrame;
	
	private int buildThickness;
	
	private int buildWeight;
	
	private int buildWaterResistance;
	
	private int buildQuality;
	
	private int buildTotal;
	
}
