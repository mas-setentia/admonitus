package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class DisplayDTO {

	private int displaySize;
	
	private int displayResolution;
	
	private int displayBezels;
	
	private int displayType;
	
	private int displayProtection;
	
	private int displayPeakBrightness;
	
	private int displayHdrSupport;
	
	private int displayPixelDensity;
	
	private int displayTotal;
}
