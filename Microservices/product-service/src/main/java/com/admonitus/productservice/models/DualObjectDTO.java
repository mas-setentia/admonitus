package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class DualObjectDTO {

	private Object key;
	
	private Object value;
}
