package com.admonitus.productservice.Util;

public class ProductServiceConstants {

	public static final String BATTERY_FILTER = "scoresList.batteryScore.batteryTotal";
	
	public static final String BUILD_FILTER = "scoresList.buildScore.buildTotal";
	
	public static final String CHARGING_FILTER = "scoresList.chargingSpeedScore.chargerTotal";
	
	public static final String DESIGN_FILTER = "scoresList.designScore.designTotal";
	
	public static final String DISPLAY_FILTER = "scoresList.displayScore.displayTotal";
	
	public static final String FRONT_CAMERA_FILTER = "scoresList.frontCameraScore.frontCameraTotal";
	
	public static final String IN_HOUSE_FILTER = "scoresList.inHouseScore.inHouseTotal";
	
	public static final String PERFORMANCE_FILTER = "scoresList.performanceScore.performanceTotal";
	
	public static final String RAM_SCORE_FILTER = "scoresList.ramScore.ramTotal";
	
	public static final String REAR_CAMERA_FILTER = "scoresList.rearCameraScore.rearCameraTotal";
	
	public static final String REFRESH_RATE_FILTER = "scoresList.refreshRateScore.refreshRateTotal";
	
	public static final String RETAILER_FILTER = "scoresList.retailerScore.retailerTotal";
	
	public static final String SOFTWARE_FILTER = "scoresList.softwareScore.softwareTotal";
	
	public static final String STORAGE_FILTER = "scoresList.storageScore.storageTotal";
	
	public static final String SCORE_TOTAL = "scoresList.scoreTotal";
	
	public static final String BATTERY_ENUM = "BATTERY";
	
	public static final String BUILD_ENUM = "BUILD";
	
	public static final String CHARGING_ENUM = "CHARGING";
	
	public static final String DESIGN_ENUM = "DESIGN";
	
	public static final String DISPLAY_ENUM = "DISPLAY";
	
	public static final String FRONT_CAMERA_ENUM = "FRONT_CAMERA";
	
	public static final String IN_HOUSE_ENUM = "INHOUSE";
	
	public static final String PERFORMANCE_ENUM = "PERFORMANCE";
	
	public static final String RAM_SCORE_ENUM = "RAM";
	
	public static final String REAR_CAMERA_ENUM = "REAR_CAMERA";
	
	public static final String REFRESH_RATE_ENUM = "REFRESH_RATE";
	
	public static final String RETAILER_ENUM = "RETAILER";
	
	public static final String SOFTWARE_ENUM = "SOFTWARE";
	
	public static final String STORAGE_ENUM = "STORAGE";
	
}
