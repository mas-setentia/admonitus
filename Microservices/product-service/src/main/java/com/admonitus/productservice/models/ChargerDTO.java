package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class ChargerDTO {

	private int chargerRating;
	
	private int wirelessChargingSupport;
	
	private int reverseCharging;
	
	private int chargerAdditionalFeatures;
	
	private int chargerTotal;
}
