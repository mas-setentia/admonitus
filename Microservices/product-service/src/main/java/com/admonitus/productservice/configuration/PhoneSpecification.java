package com.admonitus.productservice.configuration;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class PhoneSpecification {

	@Id
	private long specificationId;
	
	private String phoneModel;
	
	private String Camera;
	
	private String NetworkTechnology;
	
	private String announced;
	
	private String status;
	
	private String dimensions;
	
	private String weight;
	
	private String build;
	
	private String sim;
	
	List<String> bodyMeta;
	
	private String displayType;
	
	private String displaySize;
	
	private String displayResolution;
	
	private String displayProtection;
	
	List<String> displayMeta;
	
	private String softwareOS;
	
	private String chipset;
	
	private String CPU;
	
	private String GPU;
	
	List<String> softwareMeta;
	
	private String memoyCard;
	
	private String memoryInternal;
	
	List<String> memoryMeta;
	
	List<String> mainCameraList;
	
	private String mainCameraFeatures;
	
	private String mainCameraVideo;
	
	List<String> mainCameraMeta;
	
	List<String> frontCameraList;
	
	private String frontCameraFeatures;
	
	private String frontCameraVideo;
	
	List<String> frontCameraMeta;
	
	private String loudSpeaker;
	
	private String headPhoneJack;
	
	List<String> soundMeta;
	
	private String wlan;
	
	private String bluetooth;
	
	private String GPS;
	
	private String NFC;
	
	private String infraredPort;
	
	private String radio;
	
	private String usb;
	
	private String commsMeta;
	
	private String sensors;
	
	private String featuresMeta;
	
	private String batteryType;
	
	private String charging;
	
	List<String> batteryMeta;
	
	List<String> colors;
	
	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

}
