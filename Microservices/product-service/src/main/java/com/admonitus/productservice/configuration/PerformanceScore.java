package com.admonitus.productservice.configuration;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class PerformanceScore {

	@Id
	private long id;

	private int processor;

	private int performanceCPUClockSpeed;

	private int performanceGPU;

	private int performanceNetworkSIM;

	private int performanceNetworkWifi;

	private int performanceNetworkBluetooth;

	private int performanceSecurityBiometric;

	private int performanceCoreCount;

	private int performanceTotal;

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

}
