package com.admonitus.productservice.models;

import java.util.List;

import lombok.Data;

@Data
public class FrontCameraDTO {

	private List<CameraDTO> frontCamera;
	
	private int frontOtherCameraFeatures;
	
	private int frontImageProcessingPhoto;
	
	private int frontVideoCapablity;
	
	private int frontSpecialFeatures;
	
	private int frontImageProcessingVideo;
	
	private int frontCameraTotal;
}
