package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class RefreshRateDTO {

	private int refreshRate;
	
	private int touchSamplingRate;
	
	private int additionalDisplayFeatures;
	
	private int displayRefreshRateTotal;
}
