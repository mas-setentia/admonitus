package com.admonitus.productservice.models;

import com.admonitus.productservice.configuration.Manufacturer;

import lombok.Data;

@Data
public class ModelInformationDTO {

	private Manufacturer brandName;
	
	private DualObjectDTO modelName;
	
	private double price;
	
	private String specURL;
	
	private String officialURL;
}
