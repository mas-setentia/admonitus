package com.admonitus.productservice.configuration;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class StorageScore {

	@Id
	private long id;

	private int storageCapacity;

	private int storageClass;

	private int storageExpandableStorage;

	private int storageTotal;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getStorageCapacity() {
		return storageCapacity;
	}

	public void setStorageCapacity(int storageCapacity) {
		this.storageCapacity = storageCapacity;
	}

	public int getStorageClass() {
		return storageClass;
	}

	public void setStorageClass(int storageClass) {
		this.storageClass = storageClass;
	}

	public int getStorageExpandableStorage() {
		return storageExpandableStorage;
	}

	public void setStorageExpandableStorage(int storageExpandableStorage) {
		this.storageExpandableStorage = storageExpandableStorage;
	}

	public int getStorageTotal() {
		return storageTotal;
	}

	public void setStorageTotal(int storageTotal) {
		this.storageTotal = storageTotal;
	}

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

}
