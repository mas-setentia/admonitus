package com.admonitus.productservice.configuration;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class Scores {

	@Id
	private long scoreId;

	private BatteryScore batteryScore;

	private BuildScore buildScore;

	private ChargingSpeedScore chargingSpeedScore;

	private DesignScore designScore;

	private DisplayScore displayScore;

	private FrontCameraScore frontCameraScore;

	private PerformanceScore performanceScore;

	private RamScore ramScore;

	private SoftwareScore softwareScore;

	private StorageScore storageScore;

	private RearCameraScore rearCameraScore;

	private RefreshRateScore refreshRateScore;

	private InHouseScore inHouseScore;

	private RetailerScore retailerScore;

	private PhoneModels phoneModels;

	private Manufacturer manufacturer;

	private PhoneSpecification phoneSpecification;
	
	private Integer phoneSpecificationNumber;

	private int scoreTotal;
	
	private Double price;

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

}
