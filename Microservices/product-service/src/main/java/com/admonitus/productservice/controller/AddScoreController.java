package com.admonitus.productservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.admonitus.productservice.configuration.Manufacturer;
import com.admonitus.productservice.models.PhoneScoreDTO;
import com.admonitus.productservice.models.SavePhoneModelDTO;
import com.admonitus.productservice.service.impl.AddScoreServiceImpl;

@RestController
public class AddScoreController {

	@Autowired
	AddScoreServiceImpl addScoreServiceImpl;
	
	@CrossOrigin("http://localhost:4200")
	@PostMapping("/add-score")
	public boolean addScore(@RequestBody PhoneScoreDTO phoneScoreDTO) {
		return addScoreServiceImpl.addScore(phoneScoreDTO);
	}
	@GetMapping("/manufacturers")
	public List<Manufacturer> test() {
		return addScoreServiceImpl.getManufacturer();
	}
	
	@PostMapping("/save-phone-model")
	public ResponseEntity<Boolean> savePhoneModel(@RequestBody SavePhoneModelDTO savePhoneModelDTO) {
		return ResponseEntity.ok().body(addScoreServiceImpl.savePhoneModelDTO(savePhoneModelDTO));
	}
}
