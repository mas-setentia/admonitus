package com.admonitus.productservice.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.bson.types.ObjectId;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.admonitus.productservice.configuration.BatteryScore;
import com.admonitus.productservice.configuration.BuildScore;
import com.admonitus.productservice.configuration.ChargingSpeedScore;
import com.admonitus.productservice.configuration.DesignScore;
import com.admonitus.productservice.configuration.DisplayScore;
import com.admonitus.productservice.configuration.FrontCameraScore;
import com.admonitus.productservice.configuration.InHouseScore;
import com.admonitus.productservice.configuration.Manufacturer;
import com.admonitus.productservice.configuration.PerformanceScore;
import com.admonitus.productservice.configuration.PhoneModels;
import com.admonitus.productservice.configuration.PhoneSpecification;
import com.admonitus.productservice.configuration.RamScore;
import com.admonitus.productservice.configuration.RearCameraScore;
import com.admonitus.productservice.configuration.RefreshRateScore;
import com.admonitus.productservice.configuration.RetailerScore;
import com.admonitus.productservice.configuration.Scores;
import com.admonitus.productservice.configuration.SoftwareScore;
import com.admonitus.productservice.configuration.StorageScore;
import com.admonitus.productservice.models.PhoneScoreDTO;
import com.admonitus.productservice.models.PhoneSpecificationDTO;
import com.admonitus.productservice.models.SavePhoneModelDTO;
import com.admonitus.productservice.repositoy.ManufacturerRepository;
import com.admonitus.productservice.repositoy.PhoneModelRepository;

@Service
public class AddScoreServiceImpl {

	@Autowired
	private PhoneModelRepository phoneModelRepository;
	
	@Autowired
	private ManufacturerRepository manufacturerRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	public boolean addScore(PhoneScoreDTO phoneScoreDTO) {
		ModelMapper modelMapper = new ModelMapper();
		Optional<PhoneModels> phoneModelOptional = phoneModelRepository.findByModelName(phoneScoreDTO.getModelInformation().getModelName().toString());
		Manufacturer manuFacturer = phoneScoreDTO.getModelInformation().getBrandName();
		if(!phoneModelOptional.isPresent()) {
			PhoneModels phoneModel = new PhoneModels();
			phoneModel.setManufacturer(manuFacturer);
			phoneModel.setModelName(phoneScoreDTO.getModelInformation().getModelName().getValue().toString());
			List<Scores> phoneScores = new ArrayList<Scores>();
			Scores score = new Scores();
			BatteryScore batteryScore = modelMapper.map(phoneScoreDTO.getBatteryInformation(), BatteryScore.class);
			BuildScore buildScore = modelMapper.map(phoneScoreDTO.getBuildInformation(),BuildScore.class);
			ChargingSpeedScore chargingSpeedScore = modelMapper.map(phoneScoreDTO.getChargerInformation(),ChargingSpeedScore.class);
			DesignScore designScore = modelMapper.map(phoneScoreDTO.getDesignInformation(),DesignScore.class);
			DisplayScore displayScore = modelMapper.map(phoneScoreDTO.getDisplayInformation(),DisplayScore.class);
			FrontCameraScore frontCameraScore = modelMapper.map(phoneScoreDTO.getFrontCameraInformation(),FrontCameraScore.class);
			InHouseScore inHouseScore = modelMapper.map(phoneScoreDTO.getInHouseInformation(),InHouseScore.class);
			PerformanceScore performanceScore = modelMapper.map(phoneScoreDTO.getPerformanceInformation(),PerformanceScore.class);
			RamScore ramScore = modelMapper.map(phoneScoreDTO.getRamInformation(),RamScore.class);
			RearCameraScore rearCameaScore = modelMapper.map(phoneScoreDTO.getRearCameraInformation(),RearCameraScore.class);
			RefreshRateScore refreshRateScore = modelMapper.map(phoneScoreDTO.getRefreshRateInformation(),RefreshRateScore.class);
			RetailerScore retailerScore = modelMapper.map(phoneScoreDTO.getRetailerInformation(),RetailerScore.class);
			SoftwareScore softwareScore = modelMapper.map(phoneScoreDTO.getSoftwareInformation(),SoftwareScore.class);
			StorageScore storageScore = modelMapper.map(phoneScoreDTO.getStorageInformation(),StorageScore.class);
			score.setBatteryScore(batteryScore);
			score.setBuildScore(buildScore);
			score.setChargingSpeedScore(chargingSpeedScore);
			score.setDesignScore(designScore);
			score.setDisplayScore(displayScore);
			score.setFrontCameraScore(frontCameraScore);
			score.setPerformanceScore(performanceScore);
			score.setRamScore(ramScore);
			score.setRearCameraScore(rearCameaScore);
			score.setRefreshRateScore(refreshRateScore);
			score.setSoftwareScore(softwareScore);
			score.setStorageScore(storageScore);
			score.setInHouseScore(inHouseScore);
			score.setRetailerScore(retailerScore);
			score.setPhoneSpecificationNumber(null);
			int total=0;
			total+=batteryScore.getBatteryTotal();
			total+=buildScore.getBuildTotal();
			total+=chargingSpeedScore.getChargerTotal();
			total+=designScore.getDesignTotal();
			total+=displayScore.getDisplayTotal();
			total+=frontCameraScore.getFrontCameraTotal();
			total+=performanceScore.getPerformanceTotal();
			total+=ramScore.getRamTotal();
			total+=rearCameaScore.getRearCameraTotal();
			total+=refreshRateScore.getRefreshRateTotal();
			total+=softwareScore.getSoftwareTotal();
			total+=storageScore.getStorageTotal();
			score.setScoreTotal(total);
			score.setPrice(phoneScoreDTO.getModelInformation().getPrice());
			phoneScores.add(score);
			phoneModel.setScoresList(phoneScores);
			phoneModel.setPhoneSpecificationList(new ArrayList<PhoneSpecification>());
			phoneModelRepository.save(phoneModel);
			return true;
		} else {
			PhoneModels phoneModel = phoneModelOptional.get();
			List<Scores> phoneScores = phoneModel.getScoresList();
			if(phoneScores == null) {
				phoneScores = new ArrayList<Scores>();
			}
			phoneScores.addAll(phoneModel.getScoresList());
			Scores score = new Scores();
			BatteryScore batteryScore = modelMapper.map(phoneScoreDTO.getBatteryInformation(), BatteryScore.class);
			BuildScore buildScore = modelMapper.map(phoneScoreDTO.getBuildInformation(),BuildScore.class);
			ChargingSpeedScore chargingSpeedScore = modelMapper.map(phoneScoreDTO.getChargerInformation(),ChargingSpeedScore.class);
			DesignScore designScore = modelMapper.map(phoneScoreDTO.getDesignInformation(),DesignScore.class);
			DisplayScore displayScore = modelMapper.map(phoneScoreDTO.getDisplayInformation(),DisplayScore.class);
			FrontCameraScore frontCameraScore = modelMapper.map(phoneScoreDTO.getFrontCameraInformation(),FrontCameraScore.class);
			InHouseScore inHouseScore = modelMapper.map(phoneScoreDTO.getInHouseInformation(),InHouseScore.class);
			PerformanceScore performanceScore = modelMapper.map(phoneScoreDTO.getPerformanceInformation(),PerformanceScore.class);
			RamScore ramScore = modelMapper.map(phoneScoreDTO.getRamInformation(),RamScore.class);
			RearCameraScore rearCameaScore = modelMapper.map(phoneScoreDTO.getRearCameraInformation(),RearCameraScore.class);
			RefreshRateScore refreshRateScore = modelMapper.map(phoneScoreDTO.getRefreshRateInformation(),RefreshRateScore.class);
			RetailerScore retailerScore = modelMapper.map(phoneScoreDTO.getRetailerInformation(),RetailerScore.class);
			SoftwareScore softwareScore = modelMapper.map(phoneScoreDTO.getSoftwareInformation(),SoftwareScore.class);
			StorageScore storageScore = modelMapper.map(phoneScoreDTO.getStorageInformation(),StorageScore.class);
			score.setBatteryScore(batteryScore);
			score.setBuildScore(buildScore);
			score.setChargingSpeedScore(chargingSpeedScore);
			score.setDesignScore(designScore);
			score.setDisplayScore(displayScore);
			score.setFrontCameraScore(frontCameraScore);
			score.setPerformanceScore(performanceScore);
			score.setRamScore(ramScore);
			score.setRearCameraScore(rearCameaScore);
			score.setRefreshRateScore(refreshRateScore);
			score.setSoftwareScore(softwareScore);
			score.setStorageScore(storageScore);
			score.setInHouseScore(inHouseScore);
			score.setRetailerScore(retailerScore);
			score.setPhoneSpecificationNumber(phoneScoreDTO.getSpecificationNumber());
			if(phoneModel.getPhoneSpecificationList() != null && phoneScoreDTO.getSpecificationNumber()<phoneModel.getPhoneSpecificationList().size()) {
				score.setPhoneSpecification(phoneModel.getPhoneSpecificationList().get(phoneScoreDTO.getSpecificationNumber()));
			}
			int total=0;
			total+=batteryScore.getBatteryTotal();
			total+=buildScore.getBuildTotal();
			total+=chargingSpeedScore.getChargerTotal();
			total+=designScore.getDesignTotal();
			total+=displayScore.getDisplayTotal();
			total+=frontCameraScore.getFrontCameraTotal();
			total+=performanceScore.getPerformanceTotal();
			total+=ramScore.getRamTotal();
			total+=rearCameaScore.getRearCameraTotal();
			total+=refreshRateScore.getRefreshRateTotal();
			total+=softwareScore.getSoftwareTotal();
			total+=storageScore.getStorageTotal();
			score.setScoreTotal(total);
			phoneScores.add(score);
			phoneModel.setScoresList(phoneScores);
			phoneModelRepository.save(phoneModel);
			return true;
		}
		
	}
	
	public List<Manufacturer> getManufacturer() {
		return manufacturerRepository.findAll();
	}
	
	public List<PhoneSpecificationDTO> getPhoneSpecification(String modelId) {
		List<PhoneSpecificationDTO> specificationList = new ArrayList<PhoneSpecificationDTO>();
		ObjectId objectId = new ObjectId(modelId);
		Optional<PhoneModels> phoneModelOptional = phoneModelRepository.findByPhoneModelId(objectId);
		if(phoneModelOptional.isPresent()) {
			List<PhoneSpecification> specifications = new ArrayList<PhoneSpecification>();
			specifications = phoneModelOptional.get().getPhoneSpecificationList();
			if(specifications==null) {
				specifications = new ArrayList<PhoneSpecification>();
			}
			specifications.stream().forEach(specification -> {
				PhoneSpecificationDTO specificationDTO = new PhoneSpecificationDTO();
				specificationDTO = modelMapper.map(specification, PhoneSpecificationDTO.class);
				specificationList.add(specificationDTO);
			});
		}
		
		return specificationList;
	}
	
	public Map<String, String> getPhoneModels(String phoneString,String manufacturer) {
		
		List<PhoneModels> phoneModels= phoneModelRepository.findByModelNameLikeIgnoreCaseAndManufacturer_DisplayName(phoneString, manufacturer);
		Map<String,String> phoneModelMap = new HashMap<String, String>();
		phoneModels.stream().forEach(phone -> {
			phoneModelMap.put(phone.getPhoneModelId().toHexString(), phone.getModelName());
		});
		return phoneModelMap;
	}
	
	public PhoneSpecificationDTO savePhoneSpecification(PhoneSpecificationDTO specificationDTO) {
		
		Optional<PhoneModels> phoneModelOptional = phoneModelRepository.findByPhoneModelId(new ObjectId(specificationDTO.getPhoneModelId()));
		
		if(phoneModelOptional.isPresent()) {
			PhoneModels phoneModels = phoneModelOptional.get();
			if(phoneModels.getPhoneSpecificationList() == null) {
				phoneModels.setPhoneSpecificationList(new ArrayList<PhoneSpecification>());
			}
			PhoneSpecification phoneSpecification = modelMapper.map(specificationDTO, PhoneSpecification.class);
			phoneModels.getPhoneSpecificationList().add(phoneSpecification);
			phoneModelRepository.save(phoneModels);
		}
		
		return specificationDTO;
	}
	
	public boolean savePhoneModelDTO(SavePhoneModelDTO savePhoneModelDTO) {
		PhoneModels phoneModels = new PhoneModels();
		phoneModels.setModelName(savePhoneModelDTO.getModelName());
		phoneModels.setPhoneSpecificationList(new ArrayList<PhoneSpecification>());
		phoneModels.setManufacturer(savePhoneModelDTO.getManufacturer());
		phoneModels.setScoresList(new ArrayList<Scores>());
		PhoneModels save = phoneModelRepository.save(phoneModels);
		if(save==null) {
			return false;
		} else {
			return true;
		}
	}
}
