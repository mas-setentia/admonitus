package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class InHouseRating {

	private int customerSupport;
	
	private int serviceSupport;
	
	private int innovation;
	
	private int editorRating;
	
	private int criticRating;
	
	private int thirdPartyScore;
	
	private int exclusiveFactor;
	
	private int inHouseTotal;
}
