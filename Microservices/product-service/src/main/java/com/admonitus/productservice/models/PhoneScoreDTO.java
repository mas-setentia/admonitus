package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class PhoneScoreDTO {

	private ModelInformationDTO modelInformation;
	
	private BuildDTO buildInformation;
	
	private DesignDTO designInformation;
	
	private DisplayDTO displayInformation;
	
	private RefreshRateDTO refreshRateInformation;
	
	private PerformanceDTO performanceInformation;
	
	private RAMDTO ramInformation;
	
	private StorageDTO storageInformation;
	
	private RearCameraDTO rearCameraInformation;
	
	private FrontCameraDTO frontCameraInformation;
	
	private BatteryDTO batteryInformation;
	
	private ChargerDTO chargerInformation;
	
	private SoftwareDTO softwareInformation;
	
	private InHouseRating inHouseInformation;
	
	private RetailerDTO retailerInformation;
	
	private Integer specificationNumber;
}
