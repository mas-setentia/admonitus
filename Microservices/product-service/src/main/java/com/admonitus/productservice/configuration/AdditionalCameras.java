package com.admonitus.productservice.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdditionalCameras {

	@JsonProperty("camera_type")
	private int cameraType;
	
	@JsonProperty("camera_resolution")
	private int cameraResolution;
	
	@JsonProperty("camera_sensor_size")
	private int cameraSensorSize;
	
	@JsonProperty("camera_aperture")
	private int cameraAperture;
	
	@JsonProperty("camera_image_stablization")
	private int cameraImageStablization;
	
	@JsonProperty("additional_features")
	private String additionalFeatures;
	
//	@JsonProperty("camera_details")
//	private String cameraDetails;
	
	@JsonProperty("camera_total")
	private int cameraTotal;

	@Override
	public String toString() {
		return "AdditionalCameras [cameraType=" + cameraType + ", cameraResolution=" + cameraResolution
				+ ", cameraSensorSize=" + cameraSensorSize + ", cameraImageStablization=" + cameraImageStablization
				+ ", additionalFeatures=" + additionalFeatures + ", cameraTotal="
				+ cameraTotal + "]";
	}
	
}
