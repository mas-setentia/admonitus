package com.admonitus.productservice.models;

import com.admonitus.productservice.configuration.Manufacturer;
import com.admonitus.productservice.configuration.PhoneSpecification;
import com.admonitus.productservice.configuration.Scores;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class PhoneModelDTO {

	int id;

	String modelName;

	String variantName;

	Manufacturer manufacturer;

	PhoneSpecification phoneSpecification;

	Scores scores;

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

}
