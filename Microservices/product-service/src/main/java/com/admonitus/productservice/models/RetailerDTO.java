package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class RetailerDTO {

	private int demoUnit;
	
	private int margin;
	
	private int brandPromoter;
	
	private int schemes;
	
	private int extraCredit;
	
	private int retailerTotal;
}
