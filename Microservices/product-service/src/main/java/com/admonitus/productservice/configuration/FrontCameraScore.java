package com.admonitus.productservice.configuration;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class FrontCameraScore {

	@Id
	private long id;

	private List<AdditionalCameras> frontCamera;

	private int frontImageProcessingPhoto;

	private String frontVideoCapablity;

	private int frontImageProcessingVideo;

	private String frontOtherCameraFeatures;

	private int frontCameraTotal;

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

}
