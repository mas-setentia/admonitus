package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class StorageDTO {

	private int storageCapacity;
	
	private int expandableStorage;
	
	private int storageTotal;
}
