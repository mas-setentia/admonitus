package com.admonitus.productservice.configuration;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class InHouseScore {
	
	@Id
	private long id;

	private int customerSupport;

	private int serviceSupport;

	private int innovation;

	private int editorRating;

	private int criticRating;

	private int thirdPartyScore;

	private int exclusiveFactor;

	private int inHouseTotal;

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}
}
