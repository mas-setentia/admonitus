package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class DesignDTO {

	private int designBackPanelFinishing;
	
	private int designSimSlot;
	
	private int designHeadphoneJack;
	
	private int speakers;
	
	private int designAdditionalFeatures;
	
	private int designTotal;
}
