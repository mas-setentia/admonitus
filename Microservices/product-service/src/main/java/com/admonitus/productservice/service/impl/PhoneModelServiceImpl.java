package com.admonitus.productservice.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.admonitus.productservice.Util.ProductServiceConstants;
import com.admonitus.productservice.configuration.PhoneModels;
import com.admonitus.productservice.models.DualObjectDTO;
import com.admonitus.productservice.repositoy.PhoneModelRepository;

@Service
public class PhoneModelServiceImpl {

	@Autowired
	PhoneModelRepository phoneModelRepository;

	public List<PhoneModels> getPhoneModels(List<String> filters,String lowRange,String highRange) {
		List<Order> orders = new ArrayList<Order>();
		for (String filter : filters) {
			if (filter.equals(ProductServiceConstants.BATTERY_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.BATTERY_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.BUILD_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.BUILD_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.CHARGING_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.CHARGING_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.DESIGN_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.DESIGN_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.DISPLAY_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.DISPLAY_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.FRONT_CAMERA_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.FRONT_CAMERA_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.IN_HOUSE_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.IN_HOUSE_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.PERFORMANCE_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.PERFORMANCE_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.RAM_SCORE_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.RAM_SCORE_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.REAR_CAMERA_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.REAR_CAMERA_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.REFRESH_RATE_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.REFRESH_RATE_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.RETAILER_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.RETAILER_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.SOFTWARE_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.SOFTWARE_FILTER, true));
			}
			if (filter.equals(ProductServiceConstants.STORAGE_ENUM)) {
				orders.add(createNewOrder(ProductServiceConstants.SOFTWARE_FILTER, true));
			}
		}
		orders = orders.stream().limit(3l).collect(Collectors.toList());
		orders.add(0, createNewOrder(ProductServiceConstants.SCORE_TOTAL, true));
		Sort sort = Sort.by(orders);
		Double startPrice = Double.valueOf(lowRange);
		Double highPrice =  Double.valueOf(highRange);
		List<PhoneModels> temp = phoneModelRepository.findByScoresList_priceBetween(startPrice,highPrice,sort);
		return temp;
	}
	
	private Order createNewOrder(String property,boolean isDescending) {
		if(isDescending) {
			return new Order(Direction.DESC, property);
		} else {
			return new Order(Direction.ASC,property);
		}
	}
	
	public List<DualObjectDTO> displayPhoneModelInfo(String manufacturerName) {
		List<PhoneModels> phoneModels = null;
		if(manufacturerName.equals("")) {
			phoneModels = phoneModelRepository.findAll();
		} else {
			phoneModels = phoneModelRepository.findAllByManufacturer_DisplayName(manufacturerName);
		}
		List<DualObjectDTO> dualObjectDTOList = new ArrayList<DualObjectDTO>();
		phoneModels.stream().forEach(phoneModel -> {
			DualObjectDTO dualObjectDTO = new DualObjectDTO();
			dualObjectDTO.setKey(phoneModel.getPhoneModelId().toHexString());
			dualObjectDTO.setValue(phoneModel.getModelName());
			dualObjectDTOList.add(dualObjectDTO);
		});
		return dualObjectDTOList;
	}
}
