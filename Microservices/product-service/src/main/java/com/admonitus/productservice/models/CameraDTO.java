package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class CameraDTO {

	private int cameraType;
	
	private int cameraResolution;
	
	private int cameraAperture;
	
	private int cameraSensorSize;
	
	private int cameraImageStablization;
	
	private int cameraAdditionalFeatures;
}
