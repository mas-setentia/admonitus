package com.admonitus.productservice.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.admonitus.productservice.configuration.BatteryScore;
import com.admonitus.productservice.configuration.BuildScore;
import com.admonitus.productservice.configuration.ChargingSpeedScore;
import com.admonitus.productservice.configuration.DesignScore;
import com.admonitus.productservice.configuration.DisplayScore;
import com.admonitus.productservice.configuration.FrontCameraScore;
import com.admonitus.productservice.configuration.Manufacturer;
import com.admonitus.productservice.configuration.PerformanceScore;
import com.admonitus.productservice.configuration.PhoneModels;
import com.admonitus.productservice.configuration.PhoneSpecification;
import com.admonitus.productservice.configuration.RamScore;
import com.admonitus.productservice.configuration.RearCameraScore;
import com.admonitus.productservice.configuration.RefreshRateScore;
import com.admonitus.productservice.configuration.Scores;
import com.admonitus.productservice.configuration.SoftwareScore;
import com.admonitus.productservice.configuration.StorageScore;
import com.admonitus.productservice.models.DualObjectDTO;
import com.admonitus.productservice.repositoy.PhoneModelRepository;
import com.admonitus.productservice.service.impl.PhoneModelServiceImpl;

@RestController
@RequestMapping("/temp")
public class PhoneModelController {

	@Autowired
	PhoneModelRepository phoneModelRepository;
	
	@Autowired
	PhoneModelServiceImpl phoneModelServiceImpl;
	
	@GetMapping("/getphones")
	public PhoneModels temppile() {
		PhoneModels phoneModel = new PhoneModels();
		phoneModel.setModelName("temp");
		phoneModel.setManufacturer(new Manufacturer());
		return phoneModelRepository.save(phoneModel);
		
	}
	@GetMapping("/getphoneList")
	public List<PhoneModels> tempple() {
		return phoneModelRepository.findAll();
	}
	
	@DeleteMapping("/deletePhoneModel")
	public void deletePhoneModel(@RequestParam String modelName){
		phoneModelRepository.deleteByModelName(modelName);
	}
//	
//	@GetMapping("/getphones-filter")
//	public List<PhoneModels> getPhonesList(@RequestBody List<String> filters) {
//		return phoneModelServiceImpl.getPhoneModels(filters);
//	}
//	
	@PostMapping("/test")
	public PhoneModels test() {
		PhoneModels phoneModel = new PhoneModels();
		phoneModelRepository.save(phoneModel);
		return phoneModel;
	}
	@PostMapping("/savePhones")
	public PhoneModels savePhones() {
//		ObjectMapper mapper = new ObjectMapper();
//		
//		List<PhoneModelDTO> phoneList = new ArrayList<PhoneModelDTO>();
//		try {
//			phoneList = mapper.readValue(reqestJson,new TypeReference<List<PhoneModelDTO>>(){});
//		} catch (JsonMappingException e) {
//			// TODO Auto-generated catch block
//			System.out.println("Exception");
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			System.out.println("Exception");
//		}
		BatteryScore battery = new BatteryScore();
		BuildScore buildScore = new BuildScore();
		ChargingSpeedScore chargingSpeedScore = new ChargingSpeedScore();
		DesignScore designScore = new DesignScore();
		DisplayScore displayScore = new DisplayScore();
		FrontCameraScore frontCameraScore = new FrontCameraScore();
		PerformanceScore performanceScore = new PerformanceScore();
		RamScore ramScore = new RamScore();
		RearCameraScore rearCameraScore = new RearCameraScore();
		RefreshRateScore refreshRateScore = new RefreshRateScore();
		SoftwareScore softwareScore = new SoftwareScore();
		StorageScore storageScore = new StorageScore();
		PhoneModels phoneModels = new PhoneModels();
		Scores scores = new Scores();
		battery.setBatteryTotal(10);
		buildScore.setBuildTotal(20);
		chargingSpeedScore.setChargerTotal(30);
		designScore.setDesignTotal(40);
		displayScore.setDisplayTotal(50);
		frontCameraScore.setFrontCameraTotal(60);
		performanceScore.setPerformanceTotal(70);
		ramScore.setRamTotal(80);
		rearCameraScore.setRearCameraTotal(90);
		refreshRateScore.setRefreshRateTotal(100);
		softwareScore.setSoftwareTotal(120);
		storageScore.setStorageTotal(130);
		scores.setBatteryScore(battery);
		scores.setBuildScore(buildScore);
		scores.setChargingSpeedScore(chargingSpeedScore);
		scores.setDesignScore(designScore);
		scores.setDisplayScore(displayScore);
		scores.setFrontCameraScore(frontCameraScore);
		scores.setManufacturer(new Manufacturer());
		scores.setPerformanceScore(performanceScore);
		scores.setPhoneSpecification(new PhoneSpecification());
		scores.setRamScore(ramScore);
		scores.setRearCameraScore(rearCameraScore);
		scores.setRefreshRateScore(refreshRateScore);
		scores.setSoftwareScore(softwareScore);
		scores.setStorageScore(storageScore);
		List<Scores> scoresList = new ArrayList<Scores>();
		scoresList.add(scores);
		phoneModels.setScoresList(scoresList);
		phoneModels.setManufacturer(new Manufacturer());
		phoneModels.setPhoneSpecificationList(new ArrayList<PhoneSpecification>());
		phoneModels.setVariantName("temp");
		phoneModels.setModelName("test");
		try {
			phoneModelRepository.save(phoneModels);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return phoneModels;
	}
	
	@GetMapping("/display-phone-model")
	public ResponseEntity<List<DualObjectDTO>> displayPhoneModels(@RequestParam String manufacturerName) {
		return ResponseEntity.ok().body(phoneModelServiceImpl.displayPhoneModelInfo(manufacturerName));
	}
	
	@PostMapping("/getphones-filter")
	public ResponseEntity<List<PhoneModels>> applyFilterPhones(@RequestBody List<String> filters,@RequestParam String priceLow,@RequestParam String priceHigh) {
		return ResponseEntity.ok().body(phoneModelServiceImpl.getPhoneModels(filters,priceLow,priceHigh));
	}
}
