package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class BatteryDTO {

	private int batteryCapacity;
	
	private int batteryChargingInterface;
	
	private int batteryOptimization;
	
	private int batteryTotal;
}
