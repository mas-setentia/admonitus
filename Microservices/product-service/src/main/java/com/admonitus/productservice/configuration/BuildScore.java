package com.admonitus.productservice.configuration;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class BuildScore {

	@Id
	private long id;

	private int buildBackPanel;

	private int buildBackPanelProtection;

	private int buildFrame;

	private int buildThickness;

	private int buildWeight;

	private int buildWaterResistance;

	private int buildQuality;

	private int buildTotal;

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

}
