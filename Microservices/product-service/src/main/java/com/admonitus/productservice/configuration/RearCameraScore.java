package com.admonitus.productservice.configuration;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.Data;

@Data
public class RearCameraScore {

	@Id
	private long id;

	private int rearCameraPrimaryCameraResolution;

	private int rearCameraPCAperture;

	private int rearCameraPCSensorSize;

	private int rearCameraPCImageStablization;

	private int rearCameraSecondCameraType;

	private List<AdditionalCameras> rearCameraAdditionalCameras;

	private String rearCameraOtherFeatures;

	private int rearCameraImageProcessingPhoto;

	private String rearCameraVideoCapablity;

	private String rearCameraSlowMotionVideoCapablity;

	private String rearCameraSpecialFeatures;

	private int rearCameraNightMode;

	private int rearCameraImageProcessingVideo;

	private int rearCameraTotal;

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = "";
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonString;
	}

}
