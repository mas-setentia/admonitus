package com.admonitus.productservice.repositoy;

import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.admonitus.productservice.configuration.PhoneModels;

@Repository
public interface PhoneModelRepository extends MongoRepository<PhoneModels, Long> {

	Optional<PhoneModels> findByModelName(String modelName);
	
	List<PhoneModels> findByScoresList_priceBetween(Double priceLow,Double priceHigh,Sort sort);
	
	Optional<PhoneModels> findByPhoneModelId(ObjectId modelId);
	
	List<PhoneModels> findByModelNameLikeIgnoreCaseAndManufacturer_DisplayName(String model,String manufacturer);
	
	List<PhoneModels> findAllByManufacturer_DisplayName(String manufacturerName);
	
	boolean deleteByModelName(String modelName);
}
