package com.admonitus.productservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import com.admonitus.productservice.repositoy.PhoneModelRepository;

@SpringBootApplication
//@EnableNeo4jRepositories(basePackageClasses = PhoneModelRepository.class)
public class ProductServiceApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ProductServiceApplication.class, args);
	}

}
