package com.admonitus.productservice.models;

import lombok.Data;

@Data
public class SoftwareDTO {

	private int softwareOS;
	
	private int softwareUserInterface;
	
	private int softwareTotal;
}
