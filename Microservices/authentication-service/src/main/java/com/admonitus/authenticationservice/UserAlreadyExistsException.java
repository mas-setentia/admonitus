package com.admonitus.authenticationservice;

public class UserAlreadyExistsException extends RuntimeException {

	public UserAlreadyExistsException() {
		super("User Already Exists");
	}


}
