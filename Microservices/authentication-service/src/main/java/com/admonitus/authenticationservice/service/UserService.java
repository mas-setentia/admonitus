package com.admonitus.authenticationservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.admonitus.authenticationservice.model.User;
import com.admonitus.authenticationservice.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}
	public User getByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}
	@Transactional
	public void updateDetails(User user) {
		User newUser = userRepository.findByUserName(user.getUserName());
		newUser.setMobileNumber(user.getMobileNumber());
		newUser.setPassword(user.getPassword());
		userRepository.save(newUser);
	}

}
