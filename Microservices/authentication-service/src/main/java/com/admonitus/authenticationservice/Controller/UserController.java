package com.admonitus.authenticationservice.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admonitus.authenticationservice.UserAlreadyExistsException;
import com.admonitus.authenticationservice.model.User;
import com.admonitus.authenticationservice.service.AppUserDetailsService;
import com.admonitus.authenticationservice.service.EmailServiceImpl;
import com.admonitus.authenticationservice.service.UserConfirmationService;
import com.admonitus.authenticationservice.service.UserService;

@RestController
@RequestMapping("/stock-market-authentication")
public class UserController {
	//private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationServiceApplication.class);

	@Autowired
	UserService userService;

	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	AppUserDetailsService appUserDetailService;

	@Autowired
	UserConfirmationService userConfirmationService;
	@Autowired
	EmailServiceImpl emailServiceImpl;
	@PostMapping("/users")
	public void signup(@RequestBody User user) throws UserAlreadyExistsException {
//		LOGGER.info("Start");
		System.out.println(user);
		String password = user.getPassword();
		user.setPassword(passwordEncoder.encode(password));
		appUserDetailService.signup(user);
		String token = userConfirmationService.setTokenForConfirmation(user.getUserName());
		emailServiceImpl.send("ctstestmail10@gmail.com", user.getEmailId(), "confirm yyour email", "http://localhost:8083/authentication-service/stock-market-authentication/confirm/"+token);
//		LOGGER.info("End");
	}

	@GetMapping("/confirm/{token}")
	public void confirmMail(@PathVariable String token) {
		userConfirmationService.confirmMailAddress(token);
	}
	@PutMapping("/update-user")
	public void updateDetails(@RequestBody User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userService.updateDetails(user);
	}
	@GetMapping("/users/{userId}")
	public User getUserByName(@PathVariable String userId) {
		return userService.getByUserName(userId);
	}
//	@PutMapping("/users/new-password")
//	public void updatePassword(@RequestBody @Valid Users user)  {
//		LOGGER.info("Start");
//		String password = user.getPassword();
//		user.setPassword(passwordEncoder.encode(password));
//		userService.updateUser(user);
//		LOGGER.info("End");
//	}
//	
//	@PutMapping("/users")
//	public void updateUser(@RequestBody @Valid Users user)  {
//		LOGGER.info("Start");
//		userService.updateUser(user);
//		LOGGER.info("End");
//	}
//
//	@GetMapping("/users")
//	public List<Users> getAllUsers() {
//		return userService.getAllUsers();
//	}
//	
//	@GetMapping("/users/customers")
//	public List<Users> getAllCustomers() {
//		return userService.getAllCustomers();
//	}
//	
//	@GetMapping("/users/pending-users")
//	public List<Users> getPendingUsersList() {
//		return userService.getPendingUsersList();
//	}
//	
//	@GetMapping("/users/user-id/{userID}")
//	public Users getUserByUserID(@PathVariable String userID) {
//		return userService.getUserByUserID(userID);
//	}
//	
//	@GetMapping("/users/contact-number/{contactNumber}")
//	public Users getUserByContactNumber(@PathVariable long contactNumber) {
//		return userService.getUserByContactNumber(contactNumber);
//	}
	

}
