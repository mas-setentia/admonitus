package com.admonitus.authenticationservice.Controller;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admonitus.authenticationservice.AuthenticationServiceApplication;
import com.admonitus.authenticationservice.model.User;
import com.admonitus.authenticationservice.service.AppUserDetailsService;
import com.admonitus.authenticationservice.service.EmailServiceImpl;
import com.admonitus.authenticationservice.service.UserService;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/stock-market-authentication")
public class AuthenticationController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationServiceApplication.class);
	@Autowired
	AppUserDetailsService appUserDetailsService;
	
	@Autowired
	UserService userService;

	@Autowired
	EmailServiceImpl emailServiceImpl;
	@GetMapping("/authenticate")
	public Map<String, String> authenticate(@RequestHeader("Authorization") String authHeader) {
		LOGGER.info("Start");
		Map<String, String> data = new HashMap<>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userID = authentication.getName();
		UserDetails userDetails = appUserDetailsService.loadUserByUsername(userID);
		String userType = userDetails.getAuthorities().toArray()[0].toString();
		User user=userService.getByUserName(userID);
		data.put("token", generateJwt(getUser(authHeader)));
		data.put("userID", userID);
		data.put("Role", userType);
		data.put("confirmed", user.isConfirmed()+"");
		LOGGER.info("End");
		return data;
	}


	private String getUser(String authHeader) {
		LOGGER.info("Start");
		String[] encodedCredentials = authHeader.split("Basic ");
		String encodedMsg = "";
		if (encodedCredentials.length == 2) {
			encodedMsg = encodedCredentials[1];
		}
		String decodedMessage = new String(Base64.getDecoder().decode(encodedMsg));
		System.out.println("hajksdanhdskjnad");
		System.out.println(decodedMessage);
		LOGGER.info("End");
		return decodedMessage.split(":")[0];
	}

	private String generateJwt(String user) {
		LOGGER.info("Start");
		JwtBuilder builder = Jwts.builder();
		builder.setSubject(user);

		// Set the token issue time as current time
		builder.setIssuedAt(new Date());

		// Set the token expiry as 20 minutes from now
		builder.setExpiration(new Date((new Date()).getTime() + 1200000));
		builder.signWith(SignatureAlgorithm.HS256, "secretkey");

		String token = builder.compact();
		LOGGER.info("End");
		return token;
	}
}

