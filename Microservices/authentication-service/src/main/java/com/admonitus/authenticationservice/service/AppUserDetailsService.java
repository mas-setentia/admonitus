package com.admonitus.authenticationservice.service;

import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.admonitus.authenticationservice.UserAlreadyExistsException;
import com.admonitus.authenticationservice.model.Role;
import com.admonitus.authenticationservice.model.User;
import com.admonitus.authenticationservice.repository.UserRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	PasswordEncoder passwordEncoder;

	public AppUserDetailsService(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User user = userRepository.findByUserName(username);
		System.out.println(user.getPassword());
		AppUser appUser = null;
		if (user != null) {
			appUser = new AppUser(user);
		} else {
			throw new UsernameNotFoundException(username);
		}
		return appUser;
	}

	@Transactional
	public void signup(User user) throws UserAlreadyExistsException {
		if (userRepository.findByUserName(user.getUserName()) == null) {
			Set<Role> roleList = new HashSet<Role>();
			roleList.add(new Role(2, "USER"));
			user.setAccountType(roleList);
			userRepository.save(user);
		}
	}

}
